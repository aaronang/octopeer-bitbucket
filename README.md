# Octopeer
**Latest Build**

[![wercker status](https://app.wercker.com/status/58d7606deea2e9a573c66d7fd5f57ef4/s "wercker status")](https://app.wercker.com/project/bykey/58d7606deea2e9a573c66d7fd5f57ef4)
[![Coverage Status](https://coveralls.io/repos/bitbucket/CasBs/ooc-octopeer/badge.svg?branch=develop)](https://coveralls.io/bitbucket/CasBs/ooc-octopeer?branch=develop)
[![Dependency Status](https://www.versioneye.com/user/projects/577232b7143be60041a0ccbd/badge.svg?style=flat-square)](https://www.versioneye.com/user/projects/577232b7143be60041a0ccbd)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/918ed6d8ffd340e6977ec68fb5f90d51)](https://www.codacy.com/app/s-j-m-buijs/ooc-octopeer?utm_source=CasBs@bitbucket.org&amp;utm_medium=referral&amp;utm_content=CasBs/ooc-octopeer&amp;utm_campaign=Badge_Grade)

[![wercker status](https://app.wercker.com/status/58d7606deea2e9a573c66d7fd5f57ef4/m "wercker status")](https://app.wercker.com/project/bykey/58d7606deea2e9a573c66d7fd5f57ef4)

**Latest Release**

[![Coverage Status](https://coveralls.io/repos/bitbucket/CasBs/ooc-octopeer/badge.svg?branch=master)](https://coveralls.io/bitbucket/CasBs/ooc-octopeer?branch=master)

**This project works in combination with the following server**

[The Octopeer Server](https://github.com/theSorcerers/octopeer)

## Setting up / building
Install *NodeJS* from [here](https://nodejs.org)

Make sure *gulp* is globally installed, if not: install using npm:
```
npm install -g gulp
```
Also, in order to compile things, get *typings*:
```
npm install -g typings
```
And use it to install the typings for _chrome_ and _jasmine_:
```
typings install
```

Install other dependencies:
```
npm install
```

## Gulp
A few commands can be executed using gulp:

- *Compile* convert typescript to javascript
- *Test*: Run the test suite
- *Typescript-lint*: Run the typescript style-checker
- *Css-lint*: Run the css style-checker
- *Html-lint*: Run the html style-checker
- *Build*: Build the code, it converts .ts files into .js files

You can view the code coverage as measured in `target/assets/unit-test-coverage/html-report`

### Using the result
Load the `dest` folder in chrome:

- Go to `extensions` (or your regional equivalent)
- Make sure `Developer Mode` is enabled
- Load unpacked extension
- Choose `dest` folder

# Software Engineering Documentation

## Other documentation

- Class Diagram, 17-06-2016: [here](https://drive.google.com/file/d/0Byx_cnrHIK23MVEtWkh2cFdaMXc/view?usp=sharing)
- Coding Choices Motivation, 17-06-2016: [here](https://bitbucket.org/CasBs/ooc-octopeer/src/14d117094e215ca6085b61959040aaa534621ac9/doc/Architecture/Coding%20Choices%20Clarification.pdf?at=release%2Fsprint-8&fileviewer=file-view-default)
- Tooling Choices Motivation, 20-05-2016: [here](https://bitbucket.org/CasBs/ooc-octopeer/src/1dfb0a9efb1f193434ee81f8fc007b321540fadd/doc/Architecture/Tooling%20Choices%20Clarification.pdf?at=release%2Fsprint-5&fileviewer=file-view-default)