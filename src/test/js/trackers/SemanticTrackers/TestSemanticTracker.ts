///<reference path="../../../../../typings/index.d.ts" />
///<reference path="../../../../main/js/trackers/SemanticTrackers/SemanticTracker.d.ts" />

/**
 * Creates a test suite for the abstract class.
 * Execute this function outside your implementation test suite with the constructor of the class.
 * e.g. `SemanticTrackerTest(ImplementSemanticTracker)`;
 */
export function SemanticTrackerTest(SemanticTrackerInstance: new () => SemanticTracker): void {

    describe("The " + new SemanticTrackerInstance().getName(), function () {
        
        beforeEach(function () {
            jasmine.clock().install();
            jasmine.clock().mockDate();
            
            this.abstractTracker = new SemanticTrackerInstance();
            this.collector = jasmine.createSpyObj("collector", ["sendMessage"]);
            this.abstractTracker.withCollector(this.collector);

            this.callAddEventListener = (_: string, callback: any) => {
                callback();
            };
            
            this.element = jasmine.createSpyObj("elem", ["addEventListener"]);
            this.element0 = jasmine.createSpyObj("div", ["addEventListener"]);
            this.element1 = jasmine.createSpyObj("div2", ["addEventListener"]);

            this.element.addEventListener.and.callFake(this.callAddEventListener);
            this.element0.addEventListener.and.callFake(this.callAddEventListener);
            this.element1.addEventListener.and.callFake(this.callAddEventListener);
        });

        afterEach(function () {
            jasmine.clock().uninstall();
        });

        it("should set the collector properly.", function () {
            this.abstractTracker.registerElement(this.element0, "Comment textfield");
            
            expect(this.collector.sendMessage).toHaveBeenCalledWith({
                table: "semantic-events/",
                data: jasmine.objectContaining({
                    element_type: 501,
                })
            });
        });

        it("should register an element via a selector correctly", function () {
            this.element0.addEventListener.and.callFake(this.callAddEventListener);
            this.element1.addEventListener.and.callFake(this.callAddEventListener);

            let elements = [this.element0, this.element1];
            
            spyOn(document, "querySelectorAll").and.returnValue(elements);

            this.abstractTracker.registerElementWithSelector("", "Comment textfield");
            
            expect(this.collector.sendMessage).toHaveBeenCalledWith(jasmine.objectContaining({
                data: jasmine.objectContaining({
                    element_type: 501
                })
            }));
        });

        it("should register a list of elements via a selector correctly", function () {
            let elementSelectors: [string, string][] = [];

            elementSelectors[0] = ["selector0", "Comment textfield"];
            elementSelectors[1] = ["selector1", "Add reaction"];

            spyOn(document, "querySelectorAll").and.returnValues([this.element0], [this.element1]);

            this.abstractTracker.registerElements(elementSelectors);
            
            expect(this.collector.sendMessage).toHaveBeenCalledWith(jasmine.objectContaining({
                data: jasmine.objectContaining({
                    element_type: 501
                })
            }));
            
            expect(this.collector.sendMessage).toHaveBeenCalledWith(jasmine.objectContaining({
                data: jasmine.objectContaining({
                    element_type: 110
                })
            }));
        });

        it("should throw an error when an element type is used that does not exist", function () {
            expect(() => {
                this.abstractTracker.registerElement(this.element, "This element does not exist.");
            }).toThrowError(new RegExp("Illegal semantic element type name:"));
        });

        it("should throw an error when an event type does not exist.", function () {
            this.abstractTracker = new WrongSemanticTracker();
            
            expect(() => {
                this.abstractTracker.registerElement(this.element, "Add reaction");
            }).toThrowError(new RegExp("Illegal semantic event type name:"));
        });

        it("should filter a disabled matching mapping out.", function () {
            let testMapping: SemanticMapping[] = [{
                name: "HA",
                selector: "div",
                track: {
                    keystroke: false,
                    click: false,
                    hover: false,
                    scroll: false
                }
            }];
            spyOn(this.abstractTracker, "registerElements");

            this.abstractTracker.withMappings(testMapping);
            
            expect(this.abstractTracker.registerElements).toHaveBeenCalledWith([]);
        });

        it("should not filter an enabled matching mapping out and map it.", function () {
            let testMapping: SemanticMapping[] = [{
                name: "HA",
                selector: "div",
                track: {
                    keystroke: true,
                    click: true,
                    hover: true,
                    scroll: true
                }
            }];
            
            spyOn(this.abstractTracker, "registerElements");

            this.abstractTracker.withMappings(testMapping);
            
            expect(this.abstractTracker.registerElements).toHaveBeenCalledWith([["div", "HA"]]);
        });
    });
}

/**
 * This class is used to test conditions which can only be triggered when a implementation
 * of `SemanticTracker` is flawed.
 */
class WrongSemanticTracker
    extends SemanticTracker {

    public getName() {
        return "WrongSemanticTracker";
    }

    public shouldRegisterElement(mapping: SemanticMapping) {
        return true;
    }

    public registerElement(element: Element, eventName: string): void {
        element.addEventListener("click", () => {
            this.createMessage("This type does not exist!", eventName);
        });
    }
}