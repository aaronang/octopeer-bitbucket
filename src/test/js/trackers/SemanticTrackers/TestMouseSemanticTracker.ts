///<reference path="../../../../../typings/index.d.ts" />

import {MouseSemanticTracker} from "../../../../main/js/trackers/SemanticTrackers/MouseSemanticTracker";
import {SemanticTrackerTest} from "./TestSemanticTracker";

SemanticTrackerTest(MouseSemanticTracker);

describe("The mouse semantic tracker", function () {
    
    beforeEach(function () {
        jasmine.clock().install();
        jasmine.clock().mockDate();
        
        this.tracker = new MouseSemanticTracker();
        this.collector = jasmine.createSpyObj("collector", ["sendMessage"]);
        this.element = jasmine.createSpyObj("elem", ["addEventListener"]);
        this.tracker.withCollector(this.collector);
        this.fireEvent = {};

        this.element.addEventListener.and.callFake((event: string, callback: any) => {
            this.fireEvent[event] = callback;
        });

        this.tracker.registerElement(this.element, "Comment textfield");
    });

    afterEach(function () {
        jasmine.clock().uninstall();
    });

    it("should correctly register a mouse enter", function () {
        this.fireEvent["mouseenter"]();
        
        expect(this.collector.sendMessage).toHaveBeenCalledTimes(1);
        expect(this.collector.sendMessage).toHaveBeenCalledWith(jasmine.objectContaining({
            data: jasmine.objectContaining({
                event_type: 202
            })
        }));
    });

    it("should correctly register a mouse leave", function () {
        this.fireEvent["mouseleave"]();
        
        expect(this.collector.sendMessage).toHaveBeenCalledTimes(1);
        expect(this.collector.sendMessage).toHaveBeenCalledWith(jasmine.objectContaining({
            data: jasmine.objectContaining({
                event_type: 203
            })
        }));
    });
});