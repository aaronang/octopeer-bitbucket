///<reference path="../../../../../typings/index.d.ts" />
import {ScrollTracker} from "../../../../main/js/trackers/RawDataTrackers/ScrollTracker";
import {testTracker} from "./TestTracker";

describe("The ScrollTracker", function() {
    testTracker(ScrollTracker);

    beforeEach(function() {
        jasmine.clock().install();
        jasmine.clock().mockDate();
        
        this.collector = jasmine.createSpyObj("Collector", ["sendMessage"]);
        this.tracker = new ScrollTracker().withCollector(this.collector);

        spyOn(window, "addEventListener").and.callFake((ev: string, listener: any) => this.listener = listener);
        this.tracker.register();
    });

    afterEach(function () {
        jasmine.clock().uninstall();
    });

    it("should send a package on scroll", function() {
        window.scrollY = 40;
        window.scrollX = 60;
        this.listener();

        expect(this.collector.sendMessage).toHaveBeenCalled();
    });

    it("should only send one package on many scrolls", function() {
        window.scrollY = 40;
        window.scrollX = 60;
        this.listener();
        
        window.scrollY = 30;
        window.scrollX = 80;
        this.listener();

        expect(this.collector.sendMessage).toHaveBeenCalledWith({
            table: "mouse-scroll-events/",
            data: jasmine.objectContaining({
                viewport_x: 60,
                viewport_y: 40
            })
        });
    });
});