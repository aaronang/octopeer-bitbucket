///<reference path="../../../../../typings/index.d.ts" />

import createSpyObj = jasmine.createSpyObj;
import {ResizeTracker} from "../../../../main/js/trackers/RawDataTrackers/ResizeTracker";
import {testTracker} from "./TestTracker";

describe("The ResizeTracker", function() {
    testTracker(ResizeTracker);

    beforeEach(function () {
        jasmine.clock().install();
        jasmine.clock().mockDate();
        
        this.eventCall = <(e: any) => void> null;
        let _this = this;
        
        window.addEventListener = function(eventName: string, callback: (e: any) => void) {
            _this.eventCall = callback;
        };

        this.tracker = new ResizeTracker();
        spyOn(this.tracker, "sendData").and.callThrough();
        this.collector = createSpyObj("TrackingCollector", ["sendMessage"]);
        this.tracker.withCollector(this.collector);
        this.tracker.register();
    });

    afterEach(function() {
        jasmine.clock().uninstall();
    });

    it("should not start sending without being triggered first.", function() {
        spyOn(this, "eventCall");
        jasmine.clock().tick(5000);
        
        expect(this.eventCall).not.toHaveBeenCalled();
    });

    it("should send a resize if the screen got resized.", function() {
        this.eventCall();
        let creationTime = Date.now() / 1000;
        jasmine.clock().tick(400);

        expect(this.collector.sendMessage).toHaveBeenCalledWith({
            table: "window-resolution-events/",
                data: {
                    width: 400,
                    height: 500,
                    created_at: creationTime
                }
       });
    });

    it("should send all resize events during a resize.", function() {
        this.eventCall();
        let creationTime1 = Date.now() / 1000;
        jasmine.clock().tick(40);
        
        this.eventCall();
        let creationTime2 = Date.now() / 1000;
        jasmine.clock().tick(400);

        expect(this.collector.sendMessage).toHaveBeenCalledWith({
            table: "window-resolution-events/",
            data: {
                width: 400,
                height: 500,
                created_at: creationTime1
            }
        });
        
        expect(this.collector.sendMessage).toHaveBeenCalledWith({
            table: "window-resolution-events/",
            data: {
                width: 400,
                height: 500,
                created_at: creationTime2
            }
        });
    });
});