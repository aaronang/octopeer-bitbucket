/**
 * The data tag format of the BitBucket user.
 */
interface BitBucketUser {

    /**
     * The username of the BitBucket user.
     */
    "username": string;

    /**
     * The display name of the BitBucket user.
     */
    "displayName": string;

    /**
     * The uuid of the BitBucket user.
     */
     "uuid": string;

    /**
     * The first name of the BitBucket user.
     */
    "firstName": string;

    /**
     * The avatar url of the BitBucket user.
     */
    "avatarUrl": string;

    /**
     * The last name of the BitBucket user.
     */
    "lastName": string;

    /**
     * Whether the BitBucket user is part of a team.
     */
    "isTeam": boolean;

    /**
     * Whether ssh is enabled for the BitBucket user.
     */
    "isSshEnabled": boolean;

    /**
     * Whether kbd-shortcuts are enabled for the BitBucket user.
     */
    "isKbdShortcutsEnabled": boolean;

    /**
     * The id of the BitBucket user.
     */
    "id": number;

    /**
     * Whether the BitBucket user is authenticated.
     */
    "isAuthenticated": boolean;
}