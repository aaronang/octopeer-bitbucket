/**
 * The data tag format of the BitBucket repository.
 */
interface BitBucketRepository {
    
    /**
     * The software configuration management.
     */
    "scm": "git";

    /**
     * Whether the repository is readOnly.
     */
    "readOnly": boolean;

    /**
     * The main branch of the repo.
     */
    "mainbranch": {
        "name": string;
    };

    /**
     * The language used in this repo.
     */
    "language": string;

    /**
     * The owner of the repo.
     */
    "owner": {
        "username": string;
        "isTeam": boolean;
    };

    /**
     * The full repo url, without "bitbucket.org".
     */
    "fullslug": string;

    /**
     * The repo title.
     */
    "slug": string;

    /**
     * The repo bitbucket id.
     */
    "id": number;

    /**
     * The programming language used in this repo.
     */
    "pygmentsLanguage": string;
}