# Contributing
For making a contribution to this project, there are a few guidelines to be followed.

- Branching has to be done according to the guidelines in the Branching section of this document.
- If you find a bug, create an issue for this. (In the issue section you can read how-to)

# Branching
We have a few branches to take into account:

- *Master*: This branch contains the latest release.
- *Develop*: This is the branch you should be branching from and contains all latest updates after the latest release.

As branching model we are using git flow. This means there are only a few types of branches to be used:

- *Feature Branches*: 'feature/your-branch-name' which contains a new feature.
- *Hotfix Branches*: 'hotfix/your-branch-name' which contains a hotfix.
- *Release Branches*: 'release/a-version-number' which contains the latest release.

By contributing to this project you will always branch from *develop* and put your pull request on merging into
*develop*. You accept to use the above mentioned branching model for all code changes.

# Creating a pull request
For creating a pull request, please use the type of branch as prefix of your pull request name for easy identifying.
An example: Branch name = 'hotfix/bug-in-main' will convert into pull request title: 'hotfix: -here your title-'.
This makes prioritising pull request a lot easier for everyone.

** What to look out for when creating a pull request **
If you are creating a pull request, make sure no typescript linting errors are present in the code and that all tests
succeed, if your pull requests doesn't accomplish one of these requirements, it will not be approved/merged.

When you create a feature, don't forget to update the documentation (like the ClassDiagram) to keep the project
up-to-date.

For this project, we want to achieve at least a 95% test coverage. If your contribution lowers this amount, it will only
be accepted if it comes with a very good explanation. This explanation will also have to be added to the required
documentation.

If your contribution deviates from the programming convention, update the required documentation with a well motivated
reason. These documentations will be read and according to the documentation, it will be accepted or rejected.

Does your pull request solve an issue? Please add the link to this issue in the description of the pull request.

# Merging a pull request
A pull request will only be merged when it has at least 2 approves by verified reviewers.

# Creating an issue
When creating an issue, make sure to make the title as descriptive as possible for the problem.
Explain fully what the problem is and if possible, add an idea on how to solve it to save other programmers a lot of
time.

If you are going to solve it yourself, please assign yourself as assignee when creating this issue.

For the 'Component' section there are a few options:

- *Core*: These are the files directly in the main directory.
- *Tracker*: These are the files in the directories 'RawDataTrackers' and 'SemanticDataTrackers'.
- *Extension*: These are the files in directory 'Extension'.
- *Tests*: These are the test files.
- *Different*: This is used when no other component applies.

Choose the right component section to make the life of the person who is going to fix this issue, easier.